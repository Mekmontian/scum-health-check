package battlemetrics

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"scum-health-check/model"
)

// Interface: battlemetrics interface
type Interface interface {
	GetServerList(query string) ([]model.ServerInfo, error)
}

// BattleMetrics: implement battlemetrics
type BattleMetrics struct {
	client *http.Client
}

// NewBattleMetrics: create battlemetrics instance
func NewBattleMetrics() Interface {
	return &BattleMetrics{
		client: &http.Client{},
	}
}

// GetServerList: get server list from battlemetrics by query string
func (bm *BattleMetrics) GetServerList(query string) ([]model.ServerInfo, error) {
	path := "https://api.battlemetrics.com/servers?filter[game]=scum&filter[search]=%22" + url.QueryEscape(query) + "%22"
	resp, err := bm.client.Get(path)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	type baseResponse struct {
		Data []model.ServerInfo `json:"data"`
	}
	bResp := baseResponse{}
	err = json.Unmarshal(body, &bResp)
	if err != nil {
		return nil, err
	}
	return bResp.Data, nil
}
