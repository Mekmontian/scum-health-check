package model

// ServerInfo: server info struct model
type ServerInfo struct {
	Type       string               `json:"type"`
	ID         string               `json:"id"`
	Attributes ServerInfoAttributes `json:"attributes"`
}

// ServerInfoAttributes: server info attributes struct model
type ServerInfoAttributes struct {
	Name       string `json:"name"`
	IP         string `json:"ip"`
	Port       uint   `json:"port"`
	Players    uint   `json:"players"`
	MaxPlayers uint   `json:"maxPlayers"`
	Rank       uint   `json:"rank"`
	Status     string `json:"status"`
}
