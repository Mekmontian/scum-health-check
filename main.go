package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"scum-health-check/battlemetrics"
	"scum-health-check/cache"
	"scum-health-check/config"
	"scum-health-check/core"
	"scum-health-check/discord"
	"syscall"

	"github.com/robfig/cron/v3"
)

func main() {
	fmt.Println("scum health check started")
	cfg := config.Initialize()
	redis := cache.NewRedis(cfg.Cache.ServerName)
	battleMetrics := battlemetrics.NewBattleMetrics()
	dc := discord.NewDiscord(cfg.Discord.BotToken)
	process := core.NewCore(redis, battleMetrics, dc)
	c := cron.New()
	cronCfg := fmt.Sprintf("*/%v * * * *", cfg.Client.HealthCheckRate)
	c.AddFunc(cronCfg, func() {
		log.Println("Health checking")
		err := process.ProcessData(core.CoreConfig{
			Query:       cfg.Client.ServerName,
			DiscordChID: cfg.Discord.SingleChannelID,
		})
		if err != nil {
			log.Println(err)
		}
	})
	go c.Start()
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	<-sig
}
