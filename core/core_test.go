package core_test

import (
	"scum-health-check/_mock"
	"scum-health-check/core"
	"testing"
)

func TestEmptyCacheWillBePass(t *testing.T) {
	process := core.NewCore(&_mock.MockCacheEmpty{}, &_mock.MockBattleMetricsOnline{}, &_mock.MockDiscord{})
	err := process.ProcessData(core.CoreConfig{
		Query:       "SERVER_NAME",
		DiscordChID: "000000000000000",
	})
	if err != nil {
		t.Error(err)
	}
}
