package core

import (
	"scum-health-check/battlemetrics"
	"scum-health-check/cache"
	"scum-health-check/constant"
	"scum-health-check/discord"
	"scum-health-check/model"
)

// Interface: core interface
type Interface interface {
	ProcessData(conf CoreConfig) error
}

// Core: implement core
type Core struct {
	state cache.Interface
	btm   battlemetrics.Interface
	dc    discord.Interface
}

// CoreConfig: config value for core engine
type CoreConfig struct {
	Query       string
	DiscordChID string
}

func NewCore(state cache.Interface, btm battlemetrics.Interface, dc discord.Interface) Interface {
	return &Core{
		state: state,
		btm:   btm,
		dc:    dc,
	}
}

// ProcessData: main core process
func (c *Core) ProcessData(conf CoreConfig) error {
	serverList, err := c.btm.GetServerList(conf.Query)
	if err != nil {
		return err
	}
	var lastServer model.ServerInfo
	var curStatus string = constant.STATUS_DEAD
	for _, server := range serverList {
		lastServer = server
		if server.Attributes.Status == "online" {
			curStatus = constant.STATUS_ONLINE
			break
		}
	}
	prevStatus, err := c.state.GetServerState(conf.Query)
	if err != nil {
		return err
	}
	if prevStatus != curStatus {
		err = c.state.SetServerState(conf.Query, curStatus)
		if err != nil {
			return err
		}
		err = c.dc.SendServerStatus(lastServer, conf.DiscordChID, curStatus)
		if err != nil {
			return err
		}
	}
	return nil
}
