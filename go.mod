module scum-health-check

go 1.16

require (
	github.com/bwmarrin/discordgo v0.23.2 // indirect
	github.com/go-redis/redis/v8 v8.8.2 // indirect
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/robfig/cron/v3 v3.0.0 // indirect
)
