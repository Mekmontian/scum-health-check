package discord

import (
	"fmt"
	"log"
	"scum-health-check/constant"
	"scum-health-check/model"

	"github.com/bwmarrin/discordgo"
)

// Interface: discord interface
type Interface interface {
	SendMessage(message, channelID string) error
	SendServerStatus(serverInfo model.ServerInfo, channelID, status string) error
}

// Discord: implement discord
type Discord struct {
	session *discordgo.Session
}

// NewDiscord: create discord instance
func NewDiscord(token string) Interface {
	discord, err := discordgo.New("Bot " + token)
	if err != nil {
		log.Panic(err)
	}
	err = discord.Open()
	if err != nil {
		log.Panic(err)
	}
	return &Discord{
		session: discord,
	}
}

// SendMessage: send msg to discord channel with string
func (d *Discord) SendMessage(message, channelID string) error {
	_, err := d.session.ChannelMessageSend(channelID, message)
	return err
}

// SendServerStatus: send server status with complex embed msg
func (d *Discord) SendServerStatus(serverInfo model.ServerInfo, channelID, status string) error {
	embedColor := constant.COLOR_DANGER
	if status == constant.STATUS_ONLINE {
		embedColor = constant.COLOR_SUCCESS
	}
	fields := []*discordgo.MessageEmbedField{
		{
			Name:  "IP",
			Value: serverInfo.Attributes.IP,
		},
		{
			Name:   "Port",
			Value:  fmt.Sprintf("%v", serverInfo.Attributes.Port),
			Inline: true,
		},
		{
			Name:  "Players",
			Value: fmt.Sprintf("%v/%v", serverInfo.Attributes.Players, serverInfo.Attributes.MaxPlayers),
		},
		{
			Name:   "Rank",
			Value:  fmt.Sprintf("%v", serverInfo.Attributes.Rank),
			Inline: true,
		},
		{
			Name:  "Status",
			Value: serverInfo.Attributes.Status,
		},
	}
	msg := discordgo.MessageEmbed{
		Title:  serverInfo.Attributes.Name,
		Color:  embedColor,
		Fields: fields,
		Author: &discordgo.MessageEmbedAuthor{
			Name: "Scum Health Check",
		},
	}
	_, err := d.session.ChannelMessageSendEmbed(channelID, &msg)
	return err
}
