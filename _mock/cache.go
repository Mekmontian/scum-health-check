package _mock

import "scum-health-check/constant"

type MockCacheOnline struct {
}

func (r *MockCacheOnline) GetServerState(serverName string) (string, error) {
	return constant.STATUS_ONLINE, nil
}

func (r *MockCacheOnline) SetServerState(serverName, state string) error {
	return nil
}

type MockCacheDead struct {
}

func (r *MockCacheDead) GetServerState(serverName string) (string, error) {
	return constant.STATUS_DEAD, nil
}

func (r *MockCacheDead) SetServerState(serverName, state string) error {
	return nil
}

type MockCacheEmpty struct {
}

func (r *MockCacheEmpty) GetServerState(serverName string) (string, error) {
	return "", nil
}

func (r *MockCacheEmpty) SetServerState(serverName, state string) error {
	return nil
}
