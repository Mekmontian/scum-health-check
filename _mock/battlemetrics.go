package _mock

import "scum-health-check/model"

type MockBattleMetricsOnline struct {
}

func (m *MockBattleMetricsOnline) GetServerList(query string) ([]model.ServerInfo, error) {
	serverInfo := []model.ServerInfo{}
	serverInfo = append(serverInfo, model.ServerInfo{
		Attributes: model.ServerInfoAttributes{
			Status: "dead",
		},
	})
	serverInfo = append(serverInfo, model.ServerInfo{
		Attributes: model.ServerInfoAttributes{
			Status: "online",
		},
	})
	return serverInfo, nil
}

type MockBattleMetricsDead struct {
}

func (m *MockBattleMetricsDead) GetServerList(query string) ([]model.ServerInfo, error) {
	serverInfo := []model.ServerInfo{}
	serverInfo = append(serverInfo, model.ServerInfo{
		Attributes: model.ServerInfoAttributes{
			Status: "dead",
		},
	})
	return serverInfo, nil
}
