package _mock

import "scum-health-check/model"

type MockDiscord struct {
}

func (d *MockDiscord) SendMessage(message, channelID string) error {
	return nil
}

func (d *MockDiscord) SendServerStatus(serverInfo model.ServerInfo, channelID, status string) error {
	return nil
}
