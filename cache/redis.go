package cache

import (
	"context"

	"github.com/go-redis/redis/v8"
)

// Redis: implement redis cache
type Redis struct {
	rdb *redis.Client
	ctx context.Context
}

// NewRedis: create redis instance
func NewRedis(serverName string) Interface {
	rdb := redis.NewClient(&redis.Options{
		Addr:     serverName,
		Password: "",
		DB:       0,
	})
	return &Redis{
		rdb: rdb,
		ctx: context.Background(),
	}
}

// GetServerState: return server status as enum string
func (r *Redis) GetServerState(serverName string) (string, error) {
	val, err := r.rdb.Get(r.ctx, serverName).Result()
	if err == redis.Nil {
		return "", nil
	}
	return val, err
}

// SetServerState: set current status of server
func (r *Redis) SetServerState(serverName, state string) error {
	return r.rdb.Set(r.ctx, serverName, state, 0).Err()
}
