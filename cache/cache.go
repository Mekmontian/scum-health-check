package cache

// Interface : cache interface
type Interface interface {
	GetServerState(serverName string) (string, error)
	SetServerState(serverName string, state string) error
}
