package config

import (
	"os"

	"github.com/joho/godotenv"
)

// Config: config struct value
type Config struct {
	Client  *ClientConfig
	Cache   *CacheConfig
	Discord *DiscordConfig
}

// ClientConfig: client config struct value
type ClientConfig struct {
	HealthCheckRate string
	ServerName      string
}

// CacheConfig: cache config struct value
type CacheConfig struct {
	ServerName string
}

// DiscordConfig: discord config struct value
type DiscordConfig struct {
	BotToken        string
	SingleChannelID string
}

// Initialize: initialize config value
func Initialize() *Config {
	godotenv.Load()
	client := &ClientConfig{
		HealthCheckRate: os.Getenv("HEALTH_CHECK_RATE"),
		ServerName:      os.Getenv("SERVER_NAME"),
	}
	cache := &CacheConfig{
		ServerName: os.Getenv("CACHE_SERVER_NAME"),
	}
	discord := &DiscordConfig{
		BotToken:        os.Getenv("DISCORD_BOT_TOKEN"),
		SingleChannelID: os.Getenv("DISCORD_SINGLE_CHANNEL_ID"),
	}
	return &Config{
		Client:  client,
		Cache:   cache,
		Discord: discord,
	}
}
